
$( document ).ready(function() {
    $('ul.our-services-menu').on('click', 'li:not(.active) a', function(e) {
        e.preventDefault();
        $(this).closest("li").addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.our-services-content').removeClass('active').eq($(this).closest("li").index()).addClass('active');
    });


    $('ul.amazing-work-menu').on('click', 'li:not(.active) a', function(e) {
        e.preventDefault();
        $(this).closest("li").addClass('active').siblings().removeClass('active');
        let category = $(this).attr("data-info");
        if(category == "data-all"){
            $(".amazing-work-gallery .amazing-work-item").hide(400);
            $(".amazing-work-gallery .amazing-work-item").slice(0, 12).show(400);
        }else{
            $(".amazing-work-gallery .amazing-work-item").hide(400);
            $(".amazing-work-gallery .amazing-work-item[data-info="+category+"]").slice(0, 12).show(400);
        }
    });

     $(".amazing-work-gallery .amazing-work-item").slice(0, 12).show(400);

    $("#load-more").on('click', function (e) {
        e.preventDefault();
        let category = $(".amazing-work-menu-item.active a").attr("data-info");
        if(category == "data-all") {
            $(".amazing-work-gallery .amazing-work-item:hidden").slice(0, 12).slideDown();
        }
        else {
            $(".amazing-work-gallery .amazing-work-item[data-info="+category+"]:hidden").slice(0, 12).slideDown();
        }
    });


    $(function(){
        let slider = $('#slider');
        let sliderWrap = $('#slider ul');
        let sliderImg = $('#slider ul li');
        let prevBtm = $('#sliderPrev');
        let nextBtm = $('#sliderNext');
        let length = sliderImg.length;
        let width = sliderImg.width();
        let thumbWidth = width/length;

        sliderWrap.width(width*(length+2));

        //Set up
        slider.after('<div id="' + 'pager' + '"></div>');
        let dataVal = 1;
        sliderImg.each(
            function(){
                $(this).attr('data-img',dataVal);
                $('#pager').append('<a data-img="' + dataVal + '"><img src=' + $('img', this).attr('src') + ' width=' + thumbWidth + '></a>');
                dataVal++;
            });

        //Copy 2 images and put them in the front and at the end
        $('#slider ul li:first-child').clone().appendTo('#slider ul');
        $('#slider ul li:nth-child(' + length + ')').clone().prependTo('#slider ul');

        sliderWrap.css('margin-left', - width);
        $('#slider ul li:nth-child(2)').addClass('active');

        let imgPos = pagerPos = $('#slider ul li.active').attr('data-img');
        $('#pager a:nth-child(' +pagerPos+ ')').addClass('active');


        //Click on Pager
        $('#pager a').on('click', function() {
            pagerPos = $(this).attr('data-img');
            $('#pager a.active').removeClass('active');
            $(this).addClass('active');

            if (pagerPos > imgPos) {
                let movePx = width * (pagerPos - imgPos);
                moveNext(movePx);
            }

            if (pagerPos < imgPos) {
                let movePx = width * (imgPos - pagerPos);
                movePrev(movePx);
            }
            return false;
        });

        //Click on Buttons
        nextBtm.on('click', function(){
            moveNext(width);
            return false;
        });

        prevBtm.on('click', function(){
            movePrev(width);
            return false;
        });

        //Function for pager active motion
        function pagerActive() {
            pagerPos = imgPos;
            $('#pager a.active').removeClass('active');
            $('#pager a[data-img="' + pagerPos + '"]').addClass('active');
        }

        //Function for moveNext Button
        function moveNext(moveWidth) {
            sliderWrap.animate({
                'margin-left': '-=' + moveWidth
            }, 500, function() {
                if (imgPos==length) {
                    imgPos=1;
                    sliderWrap.css('margin-left', - width);
                }
                else if (pagerPos > imgPos) {
                    imgPos = pagerPos;
                }
                else {
                    imgPos++;
                }
                pagerActive();
            });
        }

        //Function for movePrev Button
        function movePrev(moveWidth) {
            sliderWrap.animate({
                'margin-left': '+=' + moveWidth
            }, 500, function() {
                if (imgPos==1) {
                    imgPos=length;
                    sliderWrap.css('margin-left', -(width*length));
                }
                else if (pagerPos < imgPos) {
                    imgPos = pagerPos;
                }
                else {
                    imgPos--;
                }
                pagerActive();
            });
        }

    });



    let $grid = $('.best-images-gallery').imagesLoaded(function() {
        // init Masonry after all images have loaded
        $grid.masonry({
            // options
            itemSelector: '.best-images-item',
            columnWidth: 55,
            gutter: 10
        });
    });


});
