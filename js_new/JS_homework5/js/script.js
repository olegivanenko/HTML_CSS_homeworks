let pers = {
    age: 30,
    height: 182,
    name: "Pavel",
    pets: {
        name:'Shusha',
        status: 'alive'
    }
};

function cloneObj(primeObj){
    let copy = {}
    for(let propName in primeObj){
        if(typeof primeObj[propName] === 'object'){
            console.log(primeObj[propName])
            copy[propName] = cloneObj(primeObj[propName])
        } else {
            copy[propName] = primeObj[propName]
        }
    }
    return copy
}

let copy = cloneObj(pers)
copy.pets.status = '-1'
console.log(pers)
console.log(copy)